package uz.developers.resttemplate.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.decorators.Decorators;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import uz.developers.resttemplate.dto.ErrorBaseResponse;
import uz.developers.resttemplate.exception.ConnectorException;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;

@Slf4j
public class Connector {
    private final RestTemplate restTemplate;
    private final CircuitBreaker circuitBreaker;
    private final Retry retry;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public Connector(RestTemplate restTemplate, CircuitBreakerRegistry breakerRegistry, RetryRegistry retryRegistry) {
        this.restTemplate = restTemplate;

        CircuitBreakerConfig breakerConfig = CircuitBreakerConfig.custom()
                .failureRateThreshold(50) // Opens the circuit if 50% of the calls fail
                .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.TIME_BASED)
                .slidingWindowSize(60) // The time window is 60 seconds
                .minimumNumberOfCalls(10) // Minimum 10 calls must be made in the time window before calculating the failure rate
                .waitDurationInOpenState(Duration.ofSeconds(30)) // Circuit stays open for 30 seconds before transitioning to half-open
                .permittedNumberOfCallsInHalfOpenState(3) // Allows 3 calls in half-open state to determine if the circuit should close
                .automaticTransitionFromOpenToHalfOpenEnabled(true) // Automatically transition from open to half-open after the wait duration
                .build();

        this.circuitBreaker = breakerRegistry.circuitBreaker("connector", breakerConfig);

        RetryConfig config = RetryConfig.custom()
                .maxAttempts(5)
                .waitDuration(Duration.ofSeconds(1))
                .build();

         this.retry= retryRegistry.retry("connector", config);

    }

    public Connector(RestTemplate restTemplate, CircuitBreaker circuitBreaker, Retry retry) {
        this.restTemplate = restTemplate;
        this.circuitBreaker = circuitBreaker;
        this.retry = retry;
    }

    public Connector(RestTemplate restTemplate, CircuitBreaker circuitBreaker, RetryRegistry retryRegistry) {
        this.restTemplate = restTemplate;
        this.circuitBreaker = circuitBreaker;

        RetryConfig config = RetryConfig.custom()
                .maxAttempts(5)
                .waitDuration(Duration.ofSeconds(1))
                .build();

        this.retry= retryRegistry.retry("connector", config);
    }

    public Connector(RestTemplate restTemplate, CircuitBreakerRegistry breakerRegistry, Retry retry) {
        this.restTemplate = restTemplate;

        CircuitBreakerConfig breakerConfig = CircuitBreakerConfig.custom()
                .failureRateThreshold(50) // Opens the circuit if 50% of the calls fail
                .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.TIME_BASED)
                .slidingWindowSize(60) // The time window is 60 seconds
                .minimumNumberOfCalls(10) // Minimum 10 calls must be made in the time window before calculating the failure rate
                .waitDurationInOpenState(Duration.ofSeconds(30)) // Circuit stays open for 30 seconds before transitioning to half-open
                .permittedNumberOfCallsInHalfOpenState(3) // Allows 3 calls in half-open state to determine if the circuit should close
                .automaticTransitionFromOpenToHalfOpenEnabled(true) // Automatically transition from open to half-open after the wait duration
                .build();

        this.circuitBreaker = breakerRegistry.circuitBreaker("connector", breakerConfig);

        this.retry = retry;
    }


    public <T, R> R exchangeRetry(String url,
                                  HttpMethod method,
                                  HttpHeaders headers,
                                  T request,
                                  ParameterizedTypeReference<R> typeReference,
                                  Predicate<R> predicate) {

        return retry.executeSupplier(() -> {
            R body = exchange(url, method, headers, request, typeReference);

            if (!predicate.test(body)) {
                throw new RuntimeException();
            }
            return body;
        });
    }

    /**
     * Invoke overloaded method with default "hasContent = true" parameter
     */
    public <T, R> R exchange(String url,
                             HttpMethod method,
                             HttpHeaders headers,
                             @Nullable T body,
                             ParameterizedTypeReference<R> responseType) {

        return exchange(url,
                method,
                headers,
                body,
                true,
                responseType);
    }

    /**
     * @param hasContent - indicate should response has content/body
     */
    public <T, R> R exchange(String url,
                             HttpMethod method,
                             HttpHeaders headers,
                             @Nullable T body,
                             boolean hasContent,
                             ParameterizedTypeReference<R> responseType) {
        try {

            HttpEntity<T> httpEntity = Objects.isNull(body) ? new HttpEntity<>(headers) : new HttpEntity<>(body, headers);

            ResponseEntity<R> responseEntity = Decorators
                    .ofSupplier(() -> restTemplate.exchange(url, method, httpEntity, responseType))
                    .withCircuitBreaker(circuitBreaker)
                    .withFallback(this::circuitBreakerFallback)
                    .decorate().get();

            if (hasContent && Objects.isNull(responseEntity.getBody())) {
                throw new ConnectorException(url, "An invalid (null body) response was received from: ");
            }

            return responseEntity.getBody();

        } catch (HttpStatusCodeException ex) {
            try {
                ErrorBaseResponse errorBaseResponse = objectMapper.readValue(ex.getResponseBodyAsString(), new TypeReference<>() {
                });
                throwException(url, errorBaseResponse.getMessage());
            } catch (JsonProcessingException e) {
                throwException(url, ex.getMessage());
            }
        } catch (Exception ex) {
            throwException(url, ex.getMessage());
        }
        return null;
    }

    private void throwException(String url, String message) {
        log.error("error in {}, message: {}", url, message);
        throw new ConnectorException(url, message);
    }

    private  <T> T circuitBreakerFallback(Throwable throwable) {
        throw new RuntimeException("circuit breaker is opened: " + throwable.getMessage());
    }
}
