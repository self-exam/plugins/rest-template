package uz.developers.resttemplate.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import uz.developers.resttemplate.dto.ErrorBaseResponse;

import java.util.List;

@ControllerAdvice
public class RestTemplateExceptionHandler {

    @ExceptionHandler(ConnectorException.class)
    public ResponseEntity<ErrorBaseResponse> handle(ConnectorException ex) {
        return ResponseEntity.internalServerError()
                .body(new ErrorBaseResponse(1000, ex.getUrl().concat(": ".concat(ex.getMessage())), List.of())
        );
    }
}
