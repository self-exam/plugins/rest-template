package uz.developers.resttemplate.exception;

import lombok.Getter;

@Getter
public class ConnectorException extends RuntimeException {
    private final String url;
    public ConnectorException(String url, String message) {
        super(message);
        this.url = url;
    }
}
