package uz.developers.resttemplate.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.hc.core5.http.io.SocketConfig;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.ssl.TrustStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Configuration
public class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(httpRequestFactory());
    }

    @Bean
    public HttpComponentsClientHttpRequestFactory httpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(5);
        factory.setHttpClient(httpClient());
        return factory;
    }

    @Bean
    public CloseableHttpClient httpClient() {
        return HttpClients
                .custom()
                .setConnectionManager(connectionManager())
                .evictExpiredConnections()
                .build();
    }

    @Bean
    public PoolingHttpClientConnectionManager connectionManager() {
        try {
            final TrustStrategy acceptingTrustStrategy = (x509Certificates, s) -> true;

            SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();

            SSLConnectionSocketFactory sslConnectionSocketFactory = SSLConnectionSocketFactoryBuilder.create()
                    .setSslContext(sslContext)
                    .build();

            return PoolingHttpClientConnectionManagerBuilder.create()
                    .setMaxConnTotal(250)
                    .setMaxConnPerRoute(25)
                    .setDefaultSocketConfig(SocketConfig.DEFAULT)
                    .setSSLSocketFactory(sslConnectionSocketFactory)
                    .build();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException ex) {
            log.error("error in bean creating, error message: {}", ex.getMessage());
        }
        return new PoolingHttpClientConnectionManager();
    }
}
