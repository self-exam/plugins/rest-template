package uz.developers.resttemplate.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.RetryRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import uz.developers.resttemplate.service.Connector;

@Configuration
public class ConnectorConfig {
    @Bean
    CircuitBreakerRegistry breakerRegistry(){
        return CircuitBreakerRegistry.ofDefaults();
    }

    @Bean
    RetryRegistry retryRegistry(){
        return RetryRegistry.ofDefaults();
    }

    @Bean("connector")
    Connector connector(RestTemplate restTemplate, CircuitBreakerRegistry breakerRegistry, RetryRegistry retryRegistry) {
        return new Connector(restTemplate, breakerRegistry, retryRegistry);
    }
}
