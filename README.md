# rest-template-wrapper

### `RestTemplate` bilan rest apilarni chaqirish uchun plugin, ishlatish uchun quyidagi dependencyni `pom.xml` ga qo'shish kerak

```xml
<dependency>
    <groupId>uz.developers.common</groupId>
    <artifactId>rest-template-wrapper</artifactId>
    <version>2.0.0</version>
</dependency>
```

Bu pluginni ichiga Resilience4j ning circuit-breaker va retry patternlari ham qo'llangan qo'shimcha config ham qilish mumkin, qo'shimcha configlar ham o'zi ishlaydi